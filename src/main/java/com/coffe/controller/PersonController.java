package com.coffe.controller;

import com.coffe.domain.Person;
import com.coffe.service.PersonService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

/**
 * Created by ekansh on 12/7/15.
 */

@Controller
@RequestMapping(value={"","/person"})
public class PersonController {

    @Autowired
    private PersonService personService;

/*
    @RequestMapping(value = {"/","index"})
    public String index(Model model){
        System.out.println(personService.findAll());
        model.addAttribute("persons", personService.findAll());
        return "person/index";
    }
*/
    @RequestMapping(value={"/","home"})
    public String home(){
        return "home";
    }
    @RequestMapping(value = "create")
    public String create(){
        return "create";
    }

    @RequestMapping(value = "person/save", method=RequestMethod.POST)
    public String save(Person person){
        personService.save(person);
        return "redirect:home";
    }

    @RequestMapping(value = "edit/{id}")
    public String edit(@PathVariable Long id, Model model){
        model.addAttribute("person",personService.findById(id));
        return "edit";
    }

    @RequestMapping(value = "/login", method=RequestMethod.GET)
    public String login(){
        return "login";
    }


    @RequestMapping(value = "update",method = RequestMethod.POST)
    public String update(Person person){
        personService.save(person);
        return "redirect:index";
    }


    @RequestMapping(value = "/test")
    public String test(){
        return " hello world";
    }


}
