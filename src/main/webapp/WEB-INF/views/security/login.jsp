<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
	<link type="text/css" href="/css/bootstrap.css" rel="stylesheet" />
	<link href="/css/signin.css" rel="stylesheet"type="text/css"/>
<title>Spring Security Example</title>
</head>
<body class="security-app">
	<div class="container">
	<form action="/login" method="post" class="form-signin">
		<h2 class="form-signin-heading">Proszę zaloguj się</h2>
		<label for="inputName" class="sr-only">Name</label>
				<input type="text" class="form-control" name="username"
					placeholder="User Name" id="inputName" required autofocus>
		<label for="inputPassword" class="sr-only">Password</label>
				<input type="password" class="form-control" name="password"
					placeholder="Password" required id="inputPassword"/>
		<div>
				<button class="btn btn-lg  btn-primary btn-block" type="submite">Zaloguj się</button>

			<c:if test="${param.error ne null}">
				<div class="alert-danger">Invalid username and password.</div>
			</c:if>
			<c:if test="${param.logout ne null}">
				<div class="alert-normal">You have been logged out.</div>
			</c:if>
		</div>
		<input type="hidden" name="${_csrf.parameterName}"
			value="${_csrf.token}" />
	</form>
</div>
</body>
</html>
