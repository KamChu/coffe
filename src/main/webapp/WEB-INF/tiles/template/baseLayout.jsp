<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="tiles" uri="http://tiles.apache.org/tags-tiles"%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>

<!DOCTYPE html>
<html lang="pl">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <title><tiles:insertAttribute name="title" /></title>

    <link type="text/css" href="/css/bootstrap.css" rel="stylesheet" />

</head>

<body>

<div class="container-fluid">
    <div class="header">
    <nav class="navbar navbar-inverse navbar-fixed-top">

        <div class="navbar-header">
            <a class="navbar-brand" href="#">Coffe Masters</a>
        </div>
        <ul class="nav navbar-nav">
            <li class="active"><a href="">Strona główna</a></li>
            <li><a href="#">Page 1</a>
            </li>
            <li><a href="#">Page 2</a></li>
        </ul>
        <ul class="nav navbar-nav navbar-right">
            <tiles:insertAttribute name="navigation" />
        </ul>

    </nav>
    </div>
    <div container-fluid>
        <div class="row">
            <img src="/images/coffebanner.jpg" style="height: 500px;width: 100%" alt="" class="img-responsive" />
        </div>
    </div>

    <div class="row">
        <tiles:insertAttribute name="content" />
    </div>
    <div class="footer">
        <tiles:insertAttribute name="footer" />
    </div>

</div>
</body>
</html>